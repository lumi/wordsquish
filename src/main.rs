use {
    std::{
        io::{
            self,
            prelude::*,
        },
        collections::{
            HashMap,
        },
    },
    rand::{
        Rng,
        seq::{
            SliceRandom,
        },
    },
    structopt::StructOpt,
};

/// A command-line utility that squishes words from a word list together.
///
/// Takes a newline-separated list from stdin.
#[derive(Debug, StructOpt)]
#[structopt(name = "wordsquish")]
struct Opt {
    /// The minimum length that a suffix can be.
    #[structopt(short = "s", long = "min-suffix-length", default_value = "2")]
    pub min_suffix_length: usize,
    /// The maximum length that a suffix can be.
    #[structopt(short = "S", long = "max-suffix-length", default_value = "5")]
    pub max_suffix_length: usize,
    /// The minimum amount of words in a result.
    #[structopt(short = "w", long = "min-word-count", default_value = "2")]
    pub min_word_count: usize,
    /// The maximum amount of words in a result.
    #[structopt(short = "W", long = "max-word-count", default_value = "3")]
    pub max_word_count: usize,
    /// The amount of words to generate.
    #[structopt(short = "c", long = "count", default_value = "1")]
    pub count: usize,
    /// The word to start with.
    #[structopt(name = "START WORD")]
    pub start_word: Option<String>,
}

fn suffixes(text: &str, min_len: usize, max_len: usize) -> Vec<String> {
    let mut res = Vec::new();
    let mut s = String::new();
    for c in text.chars().rev() {
        s.push(c);
        if s.len() > max_len {
            break;
        }
        if s.len() >= min_len {
            res.push(s.chars().rev().collect());
        }
    }
    res
}

fn main() {
    let opt = Opt::from_args();
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);
    let words: Vec<_> = lines.collect();
    let mut prefixes = HashMap::new();
    for (widx, word) in words.iter().enumerate() {
        let mut s = String::new();
        for c in word.chars() {
            s.push(c);
            prefixes.entry(s.clone())
                .or_insert_with(|| Vec::new())
                .push(widx);
        }
    }
    let mut rng = rand::thread_rng();
    let mut out_count = 0;
    let mut iterations = 0;
    while out_count < opt.count {
        iterations += 1;
        if iterations > 100 {
            eprintln!("failed to generate more words");
            break;
        }
        let mut res = String::new();
        let mut composition = Vec::new();
        if let Some(start_word) = &opt.start_word {
            res.push_str(start_word);
            composition.push(start_word.to_owned());
        }
        else {
            let word_idx = rng.gen_range(0, words.len());
            res.push_str(&words[word_idx]);
            composition.push(words[word_idx].clone());
        }
        let count = rng.gen_range(opt.min_word_count, opt.max_word_count + 1);
        for _ in 1..count {
            let mut suffix_list = suffixes(&res, opt.min_suffix_length, opt.max_suffix_length + 1);
            suffix_list.shuffle(&mut rng);
            for suffix in suffix_list.into_iter().rev() {
                if let Some(suff) = prefixes.get(&suffix) {
                    let suff: Vec<_> = suff.iter().map(|x| *x).collect();
                    if suff.is_empty() {
                        continue;
                    }
                    let word_idx = *suff.choose(&mut rng).unwrap();
                    let word = &words[word_idx];
                    if res == &word[..suffix.len()] {
                        break;
                    }
                    let to_add = &word[suffix.len()..];
                    if to_add.is_empty() {
                        continue;
                    }
                    res += to_add;
                    composition.push(words[word_idx].clone());
                    break;
                }
            }
        }
        if composition.len() > 1 {
            println!("{} = {}", composition.join(" + "), res);
            out_count += 1;
            iterations = 0;
        }
    }
}
